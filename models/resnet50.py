import tensorflow as tf
from PIL import Image
import matplotlib.pyplot as plt

train_datagen = tf.keras.preprocessing.image.ImageDataGenerator(
    preprocessing_function=tf.keras.applications.resnet50.preprocess_input,
    shear_range=0.2, zoom_range=0.2, brightness_range=[0.5, 1.5])

test_datagen = tf.keras.preprocessing.image.ImageDataGenerator(
    preprocessing_function=tf.keras.applications.resnet50.preprocess_input)

train_images_path = "/home/blenoclaus/study/pos-mdc/inf-0619/image-database/wave/train"
val_images_path = "/home/blenoclaus/study/pos-mdc/inf-0619/image-database/wave/validation"

train_generator = train_datagen.flow_from_directory(
    train_images_path,
    target_size=(30, 30),
    batch_size=32,
    class_mode='categorical')

images, label = train_generator.next()
print(images.shape, label.shape)

validation_generator = test_datagen.flow_from_directory(
    val_images_path,
    target_size=(30, 30),
    batch_size=32,
    class_mode='categorical',
    shuffle=False)

images, label = validation_generator.next()
print(images.shape, label.shape)

import numpy as np
from sklearn.utils.class_weight import compute_class_weight

class_weights = compute_class_weight('balanced', np.unique(train_generator.classes), train_generator.classes)
train_class_weights = dict(enumerate(class_weights))

print(train_class_weights)

################# MODEL ######################

from tensorflow.keras import layers

img_shape = (30, 30, 3)

model = tf.keras.Sequential()
model.add(layers.Conv2D(filters=15, kernel_size=(4, 4), padding='same', kernel_initializer='glorot_uniform',
                        kernel_regularizer=tf.keras.regularizers.l2(0.00001), activation='sigmoid',
                        input_shape=img_shape))
# Max pooling de tamanho 2x2
model.add(layers.AveragePooling2D(pool_size=(2, 2)))
# Camada convolucional com 10 filtros de tamanho 3x3 e ativação tanh
model.add(layers.Conv2D(filters=15, kernel_size=(3, 3), padding='same', kernel_initializer='glorot_uniform',
                        kernel_regularizer=tf.keras.regularizers.l2(0.00001), activation='sigmoid',
                        input_shape=img_shape))
# Max pooling de tamanho 2x2
model.add(layers.AveragePooling2D(pool_size=(2, 2)))
# Camada convolucional com 10 filtros de tamanho 3x3 e ativação tanh
model.add(layers.Conv2D(filters=5, kernel_size=(2, 2), padding='same', kernel_initializer='glorot_uniform',
                        kernel_regularizer=tf.keras.regularizers.l2(0.00001), activation='sigmoid',
                        input_shape=img_shape))
# Max pooling de tamanho 2x2
model.add(layers.AveragePooling2D(pool_size=(2, 2)))
model.add(layers.Flatten())
# Densa com 2 nós de saída
model.add(layers.Dense(12))

model.summary()

# Gradiente e Loss
sgd = tf.keras.optimizers.SGD(lr=0.001, momentum=0.9, decay=0.001, nesterov=True)
model.compile(loss=tf.keras.losses.CategoricalCrossentropy(from_logits=True), optimizer=sgd, metrics=['accuracy'])

model.fit(train_generator, epochs=5,class_weight=train_class_weights,  validation_data=validation_generator)