import wave
from typing import Any, Union

import matplotlib.pyplot as plt
from matplotlib.backend_bases import RendererBase
from scipy import signal
from scipy.io import wavfile
import os
import numpy as np
from PIL import Image
from scipy.fftpack import fft

audio_path = '/home/blenoclaus/study/pos-mdc/inf-0619/output-database/train/'
log_pict_Path = '/home/blenoclaus/study/pos-mdc/inf-0619/image-database/log/train/'
wave_pict_Path = '/home/blenoclaus/study/pos-mdc/inf-0619/image-database/wave/train/'

samples = []

subFolderList = []
for x in os.listdir(audio_path):
    if os.path.isdir(audio_path + '/' + x):
        subFolderList.append(x)

if not os.path.exists(log_pict_Path):
    os.makedirs(log_pict_Path)

if not os.path.exists(wave_pict_Path):
    os.makedirs(wave_pict_Path)

subFolderList = []
for x in os.listdir(audio_path):
    if os.path.isdir(audio_path + '/' + x):
        subFolderList.append(x)
        if not os.path.exists(log_pict_Path + '/' + x):
            os.makedirs(log_pict_Path + '/' + x)
        if not os.path.exists(wave_pict_Path + '/' + x):
            os.makedirs(wave_pict_Path + '/' + x)

sample_audio = []
total = 0
for x in subFolderList:
    all_files = [y for y in os.listdir(audio_path + x) if '.wav' in y]
    total += len(all_files)
    sample_audio.append(audio_path + x + '/' + all_files[0])
    print('count: %d : %s' % (len(all_files), x))
print(total)


def log_specgram(audio, sample_rate, window_size=20, step_size=10, eps=1e-10):
    nperseg = int(round(window_size * sample_rate / 1e3))
    noverlap = int(round(step_size * sample_rate / 1e3))
    freqs, _, spec = signal.spectrogram(audio,
                                        fs=sample_rate,
                                        window='hann',
                                        nperseg=nperseg,
                                        noverlap=noverlap,
                                        detrend=False)
    return freqs, np.log(spec.T.astype(np.float32) + eps)


fig_size = (30, 30)
fig = plt.figure(figsize=fig_size)


def wav2img(wav_path, targetdir='', figsize=fig_size):
    fig = plt.figure(figsize=figsize)
    samplerate, test_sound = wavfile.read(wav_path)
    _, spectrogram = log_specgram(test_sound, samplerate)
    output_file = wav_path.split('/')[-1].split('.wav')[0]
    output_file = targetdir + '/' + output_file
    plt.imsave('%s.png' % output_file, spectrogram)
    plt.close()


def wav2img_waveform(wav_path, targetdir='', figsize=fig_size):
    samplerate, test_sound = wavfile.read(wav_path)
    fig = plt.figure(figsize=figsize)
    plt.plot(test_sound)
    plt.axis('off')
    output_file = wav_path.split('/')[-1].split('.wav')[0]
    output_file = targetdir + '/' + output_file
    plt.savefig('%s.png' % output_file)
    plt.close()


for i, x in enumerate(subFolderList):
    print(i, ':', x)
    all_files = [y for y in os.listdir(audio_path + x) if '.wav' in y]
    for file in all_files:
        wav2img(audio_path + x + '/' + file, log_pict_Path + x)

for i, x in enumerate(subFolderList):
    print(i, ':', x)
    all_files = [y for y in os.listdir(audio_path + x) if '.wav' in y]
    for file in all_files:
        try:
            wav2img_waveform(audio_path + x + '/' + file, wave_pict_Path + x)
        except:
            print('fail in :' + file)
