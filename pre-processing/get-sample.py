import os
import pathlib
import shutil

classes = ['bed', 'bird', 'cat', 'five', 'off', 'right', 'three', 'go', 'marvin', 'sheila', 'down', 'left', 'silence',
           'unknown']
database_path = '/home/blenoclaus/study/pos-mdc/inf-0619/database'
split_database = '/home/blenoclaus/study/pos-mdc/inf-0619/output-database'

testing_list = database_path+'/testing_list.txt'
validation_list = database_path+'/validation_list.txt'


with open(testing_list) as testing_file:
    for testing_name in testing_file:
        (clazz, name) = testing_name.rstrip("\n").split("/")
        if clazz in classes:
            pathlib.Path(split_database+'/test/'+clazz).mkdir(parents=True, exist_ok=True)
            shutil.move(database_path+'/'+testing_name.rstrip("\n"), split_database+'/test/'+clazz+'/'+name)

with open(validation_list) as validation_file:
    for val_name in validation_file:
        (clazz, name) = val_name.rstrip("\n").split("/")
        if clazz in classes:
            pathlib.Path(split_database+'/validation/'+clazz).mkdir(parents=True, exist_ok=True)
            shutil.move(database_path+'/'+val_name.rstrip("\n"), split_database+'/validation/'+clazz+'/'+name)

for clazz in classes:
    files_path = os.listdir(database_path+'/'+clazz)
    pathlib.Path(split_database + '/train/' + clazz).mkdir(parents=True, exist_ok=True)
    for f in files_path:
        shutil.move(os.path.join(database_path+'/'+clazz,f), split_database + '/train/' + clazz + '/' + f)

